let getCube = 5 ** 3;
console.log(`The cube of 5 is ${getCube}`);

// -----------------

let address = ["2627", "1300 Roxas Blvd", "Pasay", "Metro Manila"];
let [zip, street, city, region] = address;
console.log(`The Japan embassy in the Philippines is located at ${zip}, ${street}, ${city}, ${region}.`);

// -----------------

let animal = {
		name: "Ace",
		age: 4,
		color: "Black",
		breed: "Himalayan"
	};

let {color, breed, name, age} = animal;
console.log(`Our cat is named ${name}. He is currently ${age} years old, and is a ${color.toLowerCase()} ${breed}.`);

// -----------------

let numbers = [100, 200, 300, 400, 500];

numbers.forEach(index => console.log(index));

let reduceNumber = numbers.reduce((x, y) => x + y);
console.log(reduceNumber);

// -----------------

class Dog {
	constructor(name, age, breed) {
		this.name = name,
		this.age = age,
		this.breed = breed
	};
};

let dog1 = new Dog("Ramsay", 7, "Aspin");
let dog2 = new Dog("Gordon", 2, "Aspin");
let dog3 = new Dog("FUjiko", 10, "Aspin");
console.log(dog1);
console.log(dog2);
console.log(dog3);
